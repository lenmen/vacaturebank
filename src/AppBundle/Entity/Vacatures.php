<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Vacatures
 *
 * @ORM\Table(name="vacatures", indexes={@ORM\Index(name="doelgroep_id", columns={"doelgroep_id"})})
 * @ORM\Entity
 */
class Vacatures
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="omschrijving", type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $omschrijving;

    /**
     * @var string
     *
     * @ORM\Column(name="plaats", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $plaats;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=6, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="7")
     * @Assert\Regex("~\A[1-9]\d{3} ?[a-zA-Z]{2}\z~")
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="adres", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $adres;

    /**
     * @var string
     *
     * @ORM\Column(name="huisnummer", type="string", length=5, nullable=false)
     * @Assert\NotBlank()
     */
    private $huisnummer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Doelgroepen
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Doelgroepen", inversedBy="doelgroepen", cascade="persist")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doelgroep_id", referencedColumnName="id", nullable=false)
     * })
     * @Assert\NotBlank()
     */
    public $doelgroep;



    /**
     * Set name
     *
     * @param string $name
     * @return Vacatures
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set omschrijving
     *
     * @param string $omschrijving
     * @return Vacatures
     */
    public function setOmschrijving($omschrijving)
    {
        $this->omschrijving = $omschrijving;

        return $this;
    }

    /**
     * Get omschrijving
     *
     * @return string 
     */
    public function getOmschrijving()
    {
        return $this->omschrijving;
    }

    /**
     * Set plaats
     *
     * @param string $plaats
     * @return Vacatures
     */
    public function setPlaats($plaats)
    {
        $this->plaats = $plaats;

        return $this;
    }

    /**
     * Get plaats
     *
     * @return string 
     */
    public function getPlaats()
    {
        return $this->plaats;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Vacatures
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set adres
     *
     * @param string $adres
     * @return Vacatures
     */
    public function setAdres($adres)
    {
        $this->adres = $adres;

        return $this;
    }

    /**
     * Get adres
     *
     * @return string 
     */
    public function getAdres()
    {
        return $this->adres;
    }

    /**
     * Set huisnummer
     *
     * @param string $huisnummer
     * @return Vacatures
     */
    public function setHuisnummer($huisnummer)
    {
        $this->huisnummer = $huisnummer;

        return $this;
    }

    /**
     * Get huisnummer
     *
     * @return string 
     */
    public function getHuisnummer()
    {
        return $this->huisnummer;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Vacatures
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Vacatures
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Vacatures
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set doelgroep
     *
     * @param \AppBundle\Entity\Doelgroepen $doelgroep
     * @return Vacatures
     */
    public function setDoelgroep(\AppBundle\Entity\Doelgroepen $doelgroep = null)
    {
        $this->doelgroep = $doelgroep;

        return $this;
    }

    /**
     * Get doelgroep
     *
     * @return \AppBundle\Entity\Doelgroepen 
     */
    public function getDoelgroep()
    {
        return $this->doelgroep;
    }
}
