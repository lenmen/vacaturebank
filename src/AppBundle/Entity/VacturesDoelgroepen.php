<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VacturesDoelgroepen
 *
 * @ORM\Table(name="vactures_doelgroepen")
 * @ORM\Entity
 */
class VacturesDoelgroepen
{
    /**
     * @var integer
     *
     * @ORM\Column(name="vacature_id", type="integer", nullable=false)
     */
    private $vacatureId;

    /**
     * @var integer
     *
     * @ORM\Column(name="doelgroep_id", type="integer", nullable=false)
     */
    private $doelgroepId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set vacatureId
     *
     * @param integer $vacatureId
     * @return VacturesDoelgroepen
     */
    public function setVacatureId($vacatureId)
    {
        $this->vacatureId = $vacatureId;

        return $this;
    }

    /**
     * Get vacatureId
     *
     * @return integer 
     */
    public function getVacatureId()
    {
        return $this->vacatureId;
    }

    /**
     * Set doelgroepId
     *
     * @param integer $doelgroepId
     * @return VacturesDoelgroepen
     */
    public function setDoelgroepId($doelgroepId)
    {
        $this->doelgroepId = $doelgroepId;

        return $this;
    }

    /**
     * Get doelgroepId
     *
     * @return integer 
     */
    public function getDoelgroepId()
    {
        return $this->doelgroepId;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
