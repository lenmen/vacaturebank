<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VacaturesDoelgroepen
 *
 * @ORM\Table(name="vacatures_doelgroepen", indexes={@ORM\Index(name="vacature_id", columns={"vacature_id"}), @ORM\Index(name="doelgroep_id", columns={"doelgroep_id"})})
 * @ORM\Entity
 */
class VacaturesDoelgroepen
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Vacatures
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Vacatures")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vacature_id", referencedColumnName="id")
     * })
     */
    private $vacature;

    /**
     * @var \AppBundle\Entity\Doelgroepen
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Doelgroepen")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doelgroep_id", referencedColumnName="id")
     * })
     */
    private $doelgroep;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vacature
     *
     * @param \AppBundle\Entity\Vacatures $vacature
     * @return VacaturesDoelgroepen
     */
    public function setVacature(\AppBundle\Entity\Vacatures $vacature = null)
    {
        $this->vacature = $vacature;

        return $this;
    }

    /**
     * Get vacature
     *
     * @return \AppBundle\Entity\Vacatures 
     */
    public function getVacature()
    {
        return $this->vacature;
    }

    /**
     * Set doelgroep
     *
     * @param \AppBundle\Entity\Doelgroepen $doelgroep
     * @return VacaturesDoelgroepen
     */
    public function setDoelgroep(\AppBundle\Entity\Doelgroepen $doelgroep = null)
    {
        $this->doelgroep = $doelgroep;

        return $this;
    }

    /**
     * Get doelgroep
     *
     * @return \AppBundle\Entity\Doelgroepen 
     */
    public function getDoelgroep()
    {
        return $this->doelgroep;
    }
}
