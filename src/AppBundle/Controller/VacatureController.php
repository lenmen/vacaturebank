<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Users;
use AppBundle\Entity\Vacatures;
use AppBundle\Entity\Doelgroepen;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validation; 
use Doctrine\ORM\QueryBuilder;


class VacatureController extends Controller
{
	/**
	 * Look if the user is authenticated if not redirect back
	 * @return boolean
	 */
	private function checkUser() {
		// Instance of session 
		$session = new Session();

		if(!$session->has('user_id')) {
			// Set flash bag
			$session->getFlashBag()->add('loginError', "Access Denied"); 
			return false;
		}

		return $session->get('user_id');
	}
	/**
	 * @Route("/vacatures/", name="overview")
	 */
	public function overview() {
		$session = new Session();

		if (!$user = $this->checkUser()) {
			return $this->redirectToRoute('login');
		}

		// empty variable
		$errors = null;
		$msg = null;

		if ($session->getFlashBag()->has('saveFailed')) {
			$errorsObject = $session->getFlashBag()->get('saveFailed');

			foreach($errorsObject as $err) {
				foreach($err as $e) {
					$errors[] = $e->getPropertyPath() . " : " .$e->getMessage();
					//echo $i. '<br>';
				}
			}
		}

		if ($session->getFlashBag()->has('saveSuccess')) {
			$msg = $session->getFlashBag()->get('saveSuccess');
		}

		// get all the vacatures
		$repo = $this->getDoctrine()->getRepository('AppBundle:Vacatures');
		$vacatures = $repo->findBy(["active" => '1']);

		return $this->render('AppBundle::vacatures/overview.html.twig', ["vacatures" => $vacatures, "errors" => $errors, "msg" => $msg, "user_id" => $user]);
	}

	/**
	 * @Route("/vacatures/vacature/{id}", name="vacature")
	 */
	public function getVacature($id) {
		if (!$user_id = $this->checkUser()) {
			//$session->getFlashBag()->add('loginError', "Access Denied"); 
			return $this->redirectToRoute('login');
		}

		// Create a query with the targets
		$em = $this->getDoctrine()->getManager();

		// Get the vacature and set the doelgroep
		$vacature = $em->getRepository('AppBundle:Vacatures')->findOneBy(["id" => $id, "active" => 1]);
		$doelgroep_em = $em->getRepository('AppBundle:Doelgroepen')->findOneById($vacature->doelgroep->getId());
		$vacature->setDoelgroep($doelgroep_em);
		$doelgroep = $vacature->getDoelgroep();

		// get a list of the doelgroepen
		$doelgroepen = $em->getRepository('AppBundle:Doelgroepen')->findAll();

		//return new Response(var_dump($doelgroep));

		return $this->render('AppBundle::vacatures/detail.html.twig', ["vacature" => $vacature, "doelgroep" => $doelgroep, "user_id" => $user_id]);
	}

	/**
	 * @Route("/vacatures/form/{id}", name="vacatureForm")
	 */
	public function vacatureForm($id = 0) {
		$session = new SessioN();

		if (!$user_id = $this->checkUser()) {
			//$session->getFlashBag()->add('loginError', "Access Denied"); 
			return $this->redirectToRoute('login');
		}

		// Prefined variables
		$vacature = new Vacatures();
		$errors = array();

		$em = $this->getDoctrine()->getManager();
		$doelgroep = new Doelgroepen();

		// check if the flashbag has some errors 
		if ($session->getFlashBag()->has('saveFailed')) {
			$errorsObject = $session->getFlashBag()->get('saveFailed');

			foreach($errorsObject as $err) {
				foreach($err as $e) {
					$errors[] = $e->getPropertyPath() . " : " .$e->getMessage();
					//echo $i. '<br>';
				}
			}
		}

		// If the user want to edit a record
		if ($id != 0 || $session->getFlashBag()->has('vacature_id')) { 

			$id = ($id != 0) ? $id : $session->getFlashBag()->get('vacature_id')[0];
			// Now find the vacature
			// Create a query with the targets
			$vacature = $em->getRepository('AppBundle:Vacatures')->findOneBy(["id" => $id, "active" => 1, "userId" => $user_id]);

			// Set the target group
			$doelgroep_em = $em->getRepository('AppBundle:Doelgroepen')->findOneById($vacature->doelgroep->getId());
			$vacature->setDoelgroep($doelgroep_em);
			$doelgroep = $vacature->getDoelgroep();
		}

		// Get a list of all the groups
		$doelgroepen = $em->getRepository('AppBundle:Doelgroepen')->findAll();

		if ($id != 0) {
			

		}

		// Render the page
		return $this->render('AppBundle::vacatures/form.html.twig', ["vacature" => $vacature, "doelgroep" => $doelgroep, "doelgroepen" => $doelgroepen, "vacature_id" => $id, "errors" => $errors]);
	}

	/**
	 * @Route("/vacatures/save", name="addVacature")
	 */
	public function saveVacature(Request $request) {
		$session = new Session();

		if (!$user = $this->checkUser()) {
			return $this->redirectToRoute('login');
		}

		// look if its a post event if not retur back to the overview page
		if ($request->getMethod() != "POST") {
			return $this->redirectToRoute("overview");
		}

		$vac = new Vacatures();
		$em = $this->getDoctrine()->getManager();

		// seth the values
		$vac->setName($request->get('nameInput'));
		$vac->setOmschrijving($request->get('omschrijving'));
		$vac->setPlaats($request->get('plaatsInput'));
		$vac->setPostcode($request->get('zipInput'));
		$vac->setAdres($request->get('adresInput'));
		$vac->setHuisnummer($request->get('hnInput'));
		$vac->setUserId($user);
		$vac->setCreatedAt(date('Y-m-d', strtotime("now")));

		// Set the doelgroep
		$doelgroep = $em->getRepository("AppBundle:Doelgroepen")->findOneById($request->get('doelgroep'));
		$vac->setDoelgroep($doelgroep);

		// Get an instance of the validator service
		$validator = $this->get('validator');
		$errors = $validator->validate($vac);

		if (count($errors) > 0) {
			// Set the sessionbag with the errors
			$session->getFlashBag()->add("saveFailed", $errors);

			// Check if its an edit request or an add request
			if ($request->get('vacature_id') > 0) {
				// return to edit page
				$session->getFlashBag()->add("vacature_id", $request->get('vacature_id'));
				return $this->redirectToRoute("vacatureForm");
			} 

			return $this->redirectToRoute("vacatureForm");
		}

		// Save the record into the database
		if ($request->get('vacature_id') > 0) {
			// update event
			$vacature = $em->getRepository('AppBundle:Vacatures')->find($request->get('vacature_id'));

			if (!$vacature) {
		        throw $this->createNotFoundException(
		            'No Vacature found for id '.$id
		        );
		    }

		    	// seth the values
				$vacature->setName($request->get('nameInput'));
				$vacature->setOmschrijving($request->get('omschrijving'));
				$vacature->setPlaats($request->get('plaatsInput'));
				$vacature->setPostcode($request->get('zipInput'));
				$vacature->setAdres($request->get('adresInput'));
				$vacature->setHuisnummer($request->get('hnInput'));
				$vacature->setUserId($user);

		    $em->flush();

		    // Set message
		    $session->getFlashBag()->add('saveSuccess', "The record has been changed!");

		    return $this->redirectToRoute('overview');
		}

		// Save the record
		$em->persist($vac);
		$em->flush();

		// Set message
		 $session->getFlashBag()->add('saveSuccess', "The record has been added!");

		return $this->redirectToRoute('overview');
	}

	/**
	 * @Route("/vacatures/delete/{id}", name="deleteVacature")
	 */
	public function deleteVacature($id) {
		$session = new Session();

		if (!$user = $this->checkUser()) {
			return $this->redirectToRoute('login');
		}

		// find the vacature
		$em = $this->getDoctrine()->getManager();
		$vac = $em->getRepository('AppBundle:Vacatures')->find(["id" => $id, "userId" => $user]);

		if (!$vac) {
			throw $this->createNotFoundException(
		            'No Vacature found for id '.$id
		        );
		}

		// Set active to false
		$vac->setActive(0);

		$em->flush();

		// Return to the overview page
		$session->getFlashBag()->add('saveSuccess', "Vacture has been deleted!");

		return $this->redirectToRoute('overview');
	}
}