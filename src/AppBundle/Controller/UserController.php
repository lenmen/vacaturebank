<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Users;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use JMS\Serializer\SerializationContext;

class UserController extends Controller
{
    /**
     * @Route("/", name="login")
     */
    public function indexAction(Request $request)
    {
        // create instance of the session and start it
        $session = new Session();
        //$session->start();

        // Get the errors from the flashbag set the var null the loook if there are errors
        $errors = null;

        // Look if the flashbag has an loginError attribute
        if ($session->getFlashBag()->has("loginError")) {
            $errors = $session->getFlashBag()->get("loginError");
        }

        return $this->render('AppBundle::user/login.html.twig', ["errors" => $errors]);
    }

    /**
     * Authenthicate the user 
     * @Route("/login/user", name="auth")
     */
     public function auth(Request $request) {
        // create instance of the session and start it
        $session = new Session();

        //$serializer = new SerializationContext();
       // $session->start();

        // get the input
       // $input = $request->query()->all();
        // if the method is get the redirect back with message
        if ($request->getMethod() == "GET") {
            // Set flash data
            $session->getFlashBag()->add('loginError', "Insecure request"); 

            // Redirect
            return $this->redirectToRoute('login');
        }

        $email = filter_var($request->get('email'), FILTER_VALIDATE_EMAIL); 
        $password = $request->get('password');

        // Check the inputs
        if (!$email && $password == '') {
            // Empty fields
             // Set flash data
            $session->getFlashBag()->add('loginError', "Please give the creditials"); 

            // Redirect
            return $this->redirectToRoute('login');
        } else if (!$email) {
             // Set flash data
            $session->getFlashBag()->add('loginError', "Please give email adress"); 

            // Redirect
            return $this->redirectToRoute('login');
        }

        // Encrypt the password
        $encrypted_pass = sha1(md5($password));

        // then look if the information match from the database
        $repo =  $this->getDoctrine()->getRepository('AppBundle:Users');

        $user = $repo->findOneBy(["email" => $email, "password" => $encrypted_pass]);

       // $user = $serializer->serialize($query, 'json');
        //$query = null;

        // if $user is null no user found with criteria
        if (is_null($user)) {
              // Set flash data
            $session->getFlashBag()->add('loginError', "Wrong critiria"); 

            // Redirect
            return $this->redirectToRoute('login');
        }

        // Create the auth session and redirect back
        $session->set('user_id', $user->getId());

        return $this->redirectToRoute('overview');

       // return new Response(var_dump($user->getId()));
    } 

}
